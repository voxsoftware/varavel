var controllerGroup= Varavel.Project.Modules.Welcome.Http.Controllers
export default function(Router){
	
	
	Router.get("/index", function(args){
		return Varavel.Functions(args).view("welcome::index")
	})

	Router.get("/db", controllerGroup.WelcomeController.bindMethod("pruebaDB"))

}