var Varavel= core.org.voxsoftware.Varavel
class Functions{
	constructor(args){
		this.args= args
	}
	static request(args){
		return new GlobalFunctions(args)
	}


	// Renderizar un módulo ...
	view(view, args){

		var view= new Varavel.Resources.View(this.args, view)
		return view.render(args)

	}

	public_path(){
		return global.Varavel.Project.Public ? global.Varavel.Project.Public.__dirname : ""
	}


	env(value, defaultValue){
		var val= core.org.voxsoftware.Varavel.Project.Application.current.configuration.get(value)
		if(val==undefined)
			val= defaultValue

		return val
	}
}

export default Functions