 class Collections{

   static get List(){
     return require("collections/list")
   }

   static get Array(){
     var _Array= require("collections/shim-array")
     require("collections/listen/array-changes")
     return _Array
   }

 }
 export default Collections
