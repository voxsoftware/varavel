import {EventEmitter} from 'events'
import Path from 'path'
import oUrl from 'url'
import Tls from 'tls'
var Varavel= core.org.voxsoftware.Varavel
var fsSync= core.System.IO.Fs.sync


class Server extends EventEmitter{




	get console(){
		return this.tcpServer.console
	}

	close(){
		return this.server.close()
	}


	get server(){
		return this.$server
	}

	async init(throwOnError){


		try{
			var og= require("_http_outgoing").OutgoingMessage, v
			vw.warning("Changing default funcs")
			if(!og._oo){
				
				
				var ogw= og.prototype.write
				var oge= og.prototype.end
				var ogh= og.prototype.setHeader
				og.prototype.write= function(){
					if(this.finished)
						return console.error("VARAVEL CATCH:Rejecting error after write")

					try{
						v= ogw.apply(this, arguments)
					}catch(e){
						console.error("VARAVEL CATCH: Outgoing message: ", e)
					}
					return v
				}
				
				og.prototype.setHeader= function(){
					if(this.finished)
						return console.error("VARAVEL CATCH:Rejecting error after write")

					try{
						v= ogh.apply(this, arguments)
					}catch(e){
						console.error("VARAVEL CATCH: Outgoing message: ", e)
					}
					return v
				}
				
				og.prototype.end= function(){

					try{
						v= oge.apply(this, arguments)
					}catch(e){
						console.error("VARAVEL CATCH: Outgoing message: ", e)
					}
					return v
				}
				og._oo= true
			}
		}catch(e){
			
			vw.error(e)
		}

		var c=this.console
		try{
			// Inicia el servidor ...
			var server= new core.VW.E6Html.Http.Server()
			this.$server= server
			server.timeout=this.config.get("SERVER_TIMEOUT") || 0
			var tcp= this.config.get("CREATE_TCP")==1
			server.port= tcp?0: process.env.SERVER_PORT  || this.config.get("SERVER_PORT")
			server.port= parseInt(server.port)
			server.bodyParserArgs= {
				limit: '50mb',
				extended: false
			}
			
			if(global.Varavel.Project.Public)
				server.path= global.Varavel.Project.Public.__dirname

			server.useBodyParser= this.config.get("USE_BODY_PARSER", 0)==1
			server.useBodyParser= false
			
			
			await this.initServer()
			this.emit("init")

			this.initListen()
			await server.listen()


			this.emit("listen")
			this.console.log("Server HTTP disponible: 127.0.0.1:", server.port)
			
		}
		catch(e){
			if(throwOnError)
				throw e
			this.console.error(e)
		}
	}


	async initListen(){

		/*
		while(true){
			var reqArgs= await this.$server.acceptAsync()
			this._continue(reqArgs)
		}*/

		this.$server.acceptDelegate= this._continue.bind(this)
	}

	async initServer(){

		var router= this.server.router

		/*
		var e=global.Le.middleware()
		router.use("/", function(args){
			return e(args.request, args.response, args.continue)
		})*/

		var App= global.Varavel.Project.App
		if(App.Http && App.Http.Routes){
			App.Http.Routes(router)
		}

		var Modules= global.Varavel.Project.Modules, modul
		if(Modules){
			for(var id in Modules){
				modul= Modules[id]
				if(modul.Http && modul.Http.Routes)
					await modul.Http.Routes(router)
			}
		}

	}


	async _continue(req){
		var time= new Date()
		var id= ++Server.id
		req.id= id

		try{

			//var h
			req.request.url=req.request.url.trim()

			if(!global.Varavel || global.Varavel.Funcs.env("SHOW_INREQUEST"))
				this.console.request(req.request.method, req.request.url, " ID: ", id)



			//vw.warning( req.request.headers)
			req.request.on("destroy",function(e){
				console.error("VARAVEL CATCH: secure destry: ", e)
			})

			req.request.on("error", function(e){
				console.error("VARAVEL CATCH:", e)
			})

			req.response.on("error", function(e){
				console.error("VARAVEL CATCH:", e)
			})
			await req.catch(req.continueAsync)
			this.console.info("Solicitud completa: " +  id +  " URL: ", req.request.url, " Tiempo: " + (new Date()-time) + "ms")
		}
		catch(e){

			try{

				req.response.statusCode=500
				try{req.response.write(JSON.stringify({error:e.stack},4,'\t'));}catch(ex){}
				req.response.end()
			}
			catch(ex){}
		}

		req.dispose && req.dispose()
	}

}

Server.id=0
export default Server
