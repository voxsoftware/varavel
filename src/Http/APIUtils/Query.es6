// Este archivo está designado para ayudar en los API Rest.
import Squel from 'squel'

class Query{


  constructor(modelClass){
    // Se especifica el model ...
    this.modelClass= modelClass
    this.translations={}
  }


  createQuery(args){


    var selectArg= this.translations["select"]||"fields"

    /** Ejemplod e query ..
    *
    * fields: "id,campo1,campo2"
    * joins: [
    *   "table2": {
    *      "fields": "id, campo2, campo3",
    *      "filter": [
    *         ["campo1=?", 10],
    *         {
    *            "type": "and",
    *            "conditions": ["campo2=?", 10]
    *         }
    *      ]
    *   }
    * ],
    * filter: {
    *
    * },
    * group: "campo1,campo2"
    *
    */

    var q
    if(!args.fields){
      this.defaultSelect()
      q= this.q
    }
    else{
      q= this.q= this.modelClass.select()
    }






  }

  defaultSelect(){
    this.q= this.modelClass.query()
  }

  group(value){
    if(this.validGroup && !this.validGroup(value))
      return
    this.q.group(value)
    this.$grouped= true
  }

  where(value){


    // Primero analizar si es una expresión ...
    var procesar= function(value){


      if(typeof value=="object"){
        var expr= Squel.expr(), f, arg, time=0
        if(value.type=="and"){
          f= "and"
        }
        else{
          f="or"
        }


        for(var item of value.conditions){
          arg= procesar(item)
          if(arg){
            expr[f].apply(expr, arg)
            time++
          }
        }
        if(!time)
          return null
        return [expr]
      }
      else{
        if(this.validWhere && !this.validWhere(value))
          return null
        return value
      }
    }
    var arg= procesar(value)
    if(arg)
      this.q.where(arg)
    this.$filtered= true
  }


  join(value){
    this.$joined= true
  }

  field(value){
    if(this.validField && !this.validField(value))
      return null
    this.q.field(value)
    this.$selected= true
  }

  limit(value){
    if(this.validLimit && !this.validLimit(value))
      return null
    this.q.limit(value)
    this.$limited= true
  }


  prequery(){
    // Aquí configurar opciones a realizar antes del prequery ...
  }

  postquery(){
    // Aquí configurar las opciones a realizar después de armar el query ...
  }


}
export default Query
