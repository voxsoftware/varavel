import Path from 'path'
var Fs= core.System.IO.Fs
import Cp from 'child_process'
//import LE from 'letsencrypt'
//var Le= LE.create({ server: 'staging' })

class Certificate{
	constructor(){
		this.$certs={}
	}

	async getForHostname(hostname, email){

		if(this.$certs[hostname])
			return this.$certs[hostname]

		vw.warning(hostname)
		var opts = {
			domains: [hostname], email: email || this.config.get("EMAIL") || "server@local.dev", agreeTos: true
		};

		var certs= await Le.register(opts)
		var o= {
			key: certs[0],
			cert: certs[1]
		}

		this.$certs[hostname]= o
		return o

	}


}

export default Certificate