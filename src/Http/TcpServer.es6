import Net from 'net'
var Varavel= core.org.voxsoftware.Varavel
import Tls from 'tls'
import {EventEmitter} from 'events'
//import LE from 'letsencrypt'
var LE= undefined

class TcpServer extends EventEmitter{

	async init(throwOnError){
		/*
		vw.info(LE.productionServerUrl)
		// ACME Challenge Handlers
		var leChallenge = require('le-challenge-fs').create({
		  webrootPath: '~/letsencrypt/var/'                       // or template string such as
		, debug: true                                            // '/srv/www/:hostname/.well-known/acme-challenge'
		});
		global.Le= LE.create({
			server: LE.productionServerUrl,
			challenges: { 'http-01': leChallenge }
		})
		*/
		if(this.config.get("CREATE_TCP")==1)
			await this.createConn()
		await this.renewHttpServer()
		//await this.getHttpsServer()
	}


	get httpServer(){
		if(!this.$httpServer)
			this.$httpServer= new Varavel.Http.Server()

		this.$httpServer.config= this.config
		this.$httpServer.tcpServer= this
		return this.$httpServer
	}


	async renewHttpServer(){
		// Esto permite reiniciar el servidor HTTP
		// en caso de modificación en los archivos
		// y manteniendo siempre en línea el sitio ...
		if(this.$renewing)
			return false

		this.$renewing= true
		var actual= this.$httpServer
		var newServer= new Varavel.Http.Server()
		newServer.config= this.config
		newServer.tcpServer= this
		await newServer.init()
		this.$httpServer= newServer
		if(actual)
			await actual.close()

		this.$renewing= false
		return true
	}


	get server(){
		return this.$server
	}


	createConn(){
		var task= new core.VW.Task(), server
		server= Net.createServer(this.tcpConnection.bind(this))
		server.on("error", (e)=>{

			this.console.error(e)
		})
		this.$server= server
		var port= process.env.SERVER_PORT || this.config.get("SERVER_PORT")
		port= parseInt(port)
		server.listen(port, (err)=>{
			if(err)
				task.exception= err
			this.$port= server.address().port
			this.console.log("Server TCP disponible: 127.0.0.1:", port)
			task.finish()
		})

		return task

	}

	get port(){
		if(this.$port!==undefined)
			return this.$port
			
		return this.$httpServer.$server.innerServer.address().port
	}

	get certificate(){
		if(!this.$certificate){
			this.$certificate= new Varavel.Http.Certificate(this.config)
			this.$certificate.config=this.config
		}
		return this.$certificate
	}


	async getHttpsServer(){

		// No funciona https Server

		return



		var hostname= this.config.get("HOSTNAME")|| "localhost"
		var self= this
		var iHttps= this.$httpsServer // Varavel.Http.SecureServer.getForHostname("default")


		// Por ahora esta parte funciona solo en Linux
		// Crear un certificado para el sitio
		var credentials= await self.certificate.getForHostname(hostname)
		vw.info(credentials)


		try{
			if(!iHttps){
				iHttps= new Varavel.Http.SecureServer()
				iHttps.config= this.config
				this.$httpsServer= iHttps
				//Varavel.Http.SecureServer.addForHostname("default", iHttps)


				iHttps.hostname= hostname
				iHttps.secureOptions= credentials




				// Este método permite obtener un
				iHttps.secureOptions.SNICallback= (hostname,cb)=>{

					var parts= hostname.split(".")
					if(parts.length>2){
						parts[0]= "*"
					}
					parts= parts.join(".")


					try{

						if(cb){
							self.certificate.getForHostname(parts).then(function(credentials){
								var context= Tls.createSecureContext(credentials)
								return cb(null, context)
							}).catch(function(err){
								return cb(err)
							})
						}
						else{
							var credentials2= self.certificate.getCacheForHostName(parts)
							var context= Tls.createSecureContext(credentials2)
							return context
						}
					}
					catch(e){
						self.console.error(e)
					}
				}


				setImmediate(()=>{
					iHttps.init()
				})

				await iHttps.awaitInit()
				self.httpServer.$proxy.addServer(iHttps)
				await iHttps.awaitListen()

			}
			else{
				await iHttps.awaitFirstInit()
			}

			//addr[1]= iHttps.server.port
			//addr[0]= "127.0.0.1"

		}
		catch(e){
			self.console.error(e)
			return false
		}



		return iHttps

	}




	get console(){
		if(!this.$console)
			this.$console= new Varavel.Http.Console()

		return this.$console
	}




	tcpConnection(conn) {

		//vw.log("Conexión recibida",arguments.length)
		var self= this

		conn.on("error", function(e){
			self.console.error(e)
		})

	    conn.once('data', function (buf) {
	        var address
	        var continuar= function(){
		        var proxy = Net.createConnection(address, function () {
								proxy.setTimeout && proxy.setTimeout(0)
		            proxy.write(buf, function(){
		            	conn.pipe(proxy).pipe(conn)
		            })
		        })

						proxy.on("error", function(er){
							vw.error("Error no controlado en el socket: ", er)
						})


		    }


		    var useHttps= self.config.get("USE_HTTPS")==1
	        if(!useHttps || buf[0]!=22){
	        	// usa el servidor http normalmente ...
	        	address= self.httpServer.server.port //(buf[0] === 22) ? this.httpServer : redirectAddress
	        	continuar()
	        }
	        else{



	        	self.getHttpsServer().then(function(httpsServer){

	        		address= httpsServer.server.port
	        		//vw.info(address)
	        		continuar()
	        	}).catch(function(err){
	        		vw.error(err)
	        	})
	        }


	    })
	}





}
export default TcpServer
