import {EventEmitter} from 'events'
import Path from 'path'
import oUrl from 'url'
import Tls from 'tls'
var Varavel= core.org.voxsoftware.Varavel
var fsSync= core.System.IO.Fs.sync


class Server extends EventEmitter{

	

		
	

	awaitListen(){
		var task= new core.VW.Task()
		this.once("listen", function(){
			task.finish()
		})

		return task
	}

	awaitFirstInit(){
		if(this.$inited)
			return 

		return this.awaitInit()
	}


	awaitInit(){
		var task= new core.VW.Task()
		this.once("init", function(){
			task.finish()
		})

		return task
	}


	get console(){
		return this.tcpServer.console
	}

	close(){
		return this.server.close()
	}
	

	get server(){
		return this.$server
	}

	async init(throwOnError){
		var c=this.console
		try{
			// Inicia el servidor ...
			var server= new core.VW.E6Html.Http.SecureServer(this.secureOptions)
			this.$server= server
			server.timeout=this.config.get("SERVER_TIMEOUT") || 4500
			server.port= 0
			

			server.useBodyParser= true
			this.initServer()
			this.emit("init")
			await server.listen()

			
			this.emit("listen")
			this.console.log("Server HTTPS disponible: 127.0.0.1:", server.port)
			this.initListen()
		}
		catch(e){
			if(throwOnError)
				throw e
			this.console.error(e)
		}
	}


	async initListen(){
		while(true){
			var reqArgs= await this.$server.acceptAsync()
			this._continue(reqArgs)
		}
	}

	initServer(){
		
		var router= this.server.router
		var App= global.Varavel.Project.App
		if(App.Http && App.Http.Routes){
			App.Http.Routes(router)
		}

		var Modules= global.Varavel.Project.Modules, modul
		if(Modules){
			for(var id in Modules){
				modul= Modules[id]
				if(modul.Http && modul.Http.Routes)
					modul.Http.Routes(router)
			}
		}

	}


	async _continue(req){
		var time= new Date()
		var id= ++Server.id
		req.id= id
		
		try{

			//var h
			req.request.url=req.request.url.trim()
			this.console.request(req.request.method, req.request.url, " ID: ", id)
			await req.catch(req.continue)
			this.console.info("Solicitud HTTPS completa: ", id, " Tiempo: ", new Date()-time, "ms")
		}
		catch(e){

			try{

				req.response.statusCode=500
				try{req.response.write(JSON.stringify({error:e.stack},4,'\t'));}catch(ex){}
				req.response.end()
			}
			catch(ex){}
		}
	}

}

Server.id=0
export default Server