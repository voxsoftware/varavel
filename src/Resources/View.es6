
var Varavel= core.org.voxsoftware.Varavel
class View{
	constructor(args, view){
		this.args= args
		this.view= view
	}


	// Renderizar un módulo ...
	render(args){

		var parts= this.view.split("::")
		var module, view, viewManager, path
		if(parts.length==1){

			view= parts[0]
			view= view.replace(/\./ig, function(){return "/"})
			try{
				path= Varavel.Project.App.Resources.Views.__dirname
			}
			catch(e){
				throw new Varavel.Resources.ViewNotFoundException(this.view, e)
			}

		}

		else{

			module= parts[0]
			view= parts[1]
			view= view.replace(/\./ig, function(){return "/"})
			try{
				path= global.Varavel.Modules[module].Resources.Views.__dirname
			}
			catch(e){
				throw new Varavel.Resources.ViewNotFoundException(this.view, e)
			}
		}
		
		vw.warning(path)
		viewManager= Varavel.Project.Application.current.tcpServer.httpServer.server.viewManager(path)
		vw.info(view)
		return viewManager.render(view, this.args, {
			arguments: args
		})
		

	}

}

export default View