
import mysql from 'mysql'
import Squel from 'squel'
var Varavel= core.org.voxsoftware.Varavel
var inited= false
class Database extends Varavel.Database.Database{


	constructor(config){

		// Inicia el motor a partir de la configuración ...
		this.$mysql= mysql.createPool({
		  connectionLimit : config.get("DB_CONNECTIONLIMIT")|| 10,
		  host            : config.get("DB_HOST")||'127.0.0.1',
		  user            : config.get("DB_USER")|| "root",
		  password        : config.get("DB_PASSWORD") || "",
		  port        	  : config.get("DB_PORT")|| 3306,
		  database        : config.get("DB_DATABASE")|| 3306
		});

		this.useUTC= true


		if(!inited){

			// Añadir a SQUEL un tratamiento de Fechas ...
			Squel.registerValueHandler(Date, function(value){
				var m= core.VW.Moment(value)
				var str= 'DD.MM.YYYY HH.mm.ss'
				return Squel.expr(`STR_TO_DATE('${str}', '%d.%m.%Y %H.%i.%s')`)
			})
			Squel.registerValueHandler(core.VW.Moment, function(m){
				//var m= core.VW.Moment(value)
				var str= 'DD.MM.YYYY HH.mm.ss'
				return Squel.expr(`STR_TO_DATE('${str}', '%d.%m.%Y %H.%i.%s')`)
			})
			inited= true

		}



	}


	static get mysql(){
		if(Database.$mysql)
			return Database.$mysql
		return Database.$mysql= require("mysql")
	}

	get nowExpression(){
		if(this.useUTC)
			return this.nowUTCExpression
		return Squel.str("now()")
	}

	get nowUTCExpression(){
		return Squel.str("utc_timestamp()")
	}


	get queryBuilder(){
		return new Varavel.Database.MySql.QueryBuilder(this)
	}


	executeQuery(queryBuilder){
		var param= queryBuilder.toParam?queryBuilder.toParam(): queryBuilder.toString()
		if(param.text)
			param.sql= param.text

		var task= new core.VW.Task()
		this.$mysql.query(param, function(err, results, fields){

			if(err)
				task.exception= err

			var o= {}
			o.rows= results
			o.fields= fields
			task.result= o
			task.finish()

		})
		return task
	}

}

export default Database
