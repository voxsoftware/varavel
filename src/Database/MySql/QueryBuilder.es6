import Squel from 'squel'
var Varavel= core.org.voxsoftware.Varavel
var none= Varavel.Database.MySql.SquelCreate
class QueryBuilder extends Varavel.Database.QueryBuilder{
	_engine(){
		return Squel.useFlavour("mysql")
	}

	create(){
		this.$sql= Squel.create()
	}
}
export default QueryBuilder