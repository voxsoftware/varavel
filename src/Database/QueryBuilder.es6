import Squel from 'squel'
var VaravelNamespace= core.org.voxsoftware.Varavel

class QueryBuilder{

	constructor(db){
		this.db= db
	}
	select(){
		this.$sql= this._engine().select()
		return this
	}

	create(){
		throw new core.System.NotImplementedException()
	}

	update(){
		this.$sql= this._engine().update()
		return this
	}

	delete(){
		this.$sql= this._engine().delete()
		return this
	}

	insert(){
		this.$sql= this._engine().insert()
		return this
	}

	_engine(){
		return Squel
	}

	async first(){
		return (await this.get())[0]
	}

	async get(){
		//throw new core.System.NotImplementedException()

		var result=  await this.db.executeQuery(this)
		var rows= result.rows
		var ret= new VaravelNamespace.Utils.Collections.Array()
		if(rows){
			for(var row of rows){
				ret.push(this.modelClass.__fill(row))
			}
		}
		return ret

	}

	execute(){
		throw new core.System.NotImplementedException()
	}

	clone(){
		var sql= this.$sql.clone()
		var d= Object.getPrototypeOf ? Object.getPrototypeOf(this) : this.__proto__
		var c= new d()
		c.$sql= sql
		return c
	}

	static processArguments(args){
		for(var i=0;i<args.length;i++){
			if(args[i] instanceof QueryBuilder)
				args[i]= args[i].$sql
		}
		return args
	}

}


var props= ["where", "toParam", "toString", "into", "from", "table", "returning",
	"distinct", "field", "join", "outer_join", "left_join", "right_join", "expr", "having", "group",
	"limit", "offset", "union", "function", "set", "setFields", "order", "and", "or", "with", "str"]


var createFunc= (prop)=>{
	//vw.warning(prop)
	QueryBuilder.prototype[prop]= function(){
		var func=this.$sql[prop]
		var args= QueryBuilder.processArguments(arguments)

		var r= func.apply(this.$sql, args)
		if(r==this.$sql)
			r= this

		return r
	}
}


props.forEach(createFunc)
export default QueryBuilder
