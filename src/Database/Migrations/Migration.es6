
var VaravelNamespace= core.org.voxsoftware.Varavel
var Migrations=VaravelNamespace.Database.Migrations

class Migration{
	

	up(){}
	down(){}


	async run(){

		var migration=await Migrations.Entities.Migration.select()
			.where("migration",  Object.getPrototypeOf(this).__filename)
			.first()

		if(!migration || !migration.batch==0){

			if(!migration)
				migration= new Migrations.Entities.Migration()

			await this.up()
			migration.set("migration", Object.getPrototypeOf(this).__filename)
			migration.set("batch", 1)
			await migration.save()
		}	

	}


	async remove(){


		
		var migration= await Migrations.Entities.Migration.select()
			.where("migration",  Object.getPrototypeOf(this).__filename)
			.first()

		if(migration){

			await this.down()
			migration.set("batch",0)
			await migration.save()

		}


	}


	async verifyTable(){

		var migration, create
		// Si no existe crear tabla de migración ...
		try{
			migration= Migrations.Entities.Migration.find(1)
		}
		catch(e){
			create= true
		}


		// Crear la tabla ...
		var query= VaravelNamespace.Database.Database.current.queryBuilder
		await query.create().table("migration")
			.field("migration", "varchar(400)")
			.field("batch", "integer")
			.execute()

	}

}
export default Migration