
var Varavel= core.org.voxsoftware.Varavel
class Database{
	

	static get current(){

		var config= Varavel.Project.Configuration.default
		var tipo= config.get("DB_CONNECTION")

		if(tipo=="mysql"){
			return new Varavel.Database.MySql.Database(config)
		}
		else{
			throw new core.System.NotImplementedException(tipo)
		}
	}



	get nowExpression(){
		if(this.useUTC)
			return this.nowUTCExpression
		return new Date()
	}

	get nowUTCExpression(){
		return (new Date()).toISOString()
	}


	get queryBuilder(){
		return new Varavel.Database.QueryBuilder(this)
	}

	getQueryBuilder(model){
		var e= this.queryBuilder
		e.modelClass= model
		return e
	}
	

	executeQuery(){
		throw new core.System.NotImplementedException()
	}

}

export default Database