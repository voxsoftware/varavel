

var Varavel= core.org.voxsoftware.Varavel
class Model{

	constructor(){
		this.$data= {}

		if(this.fillable){
			var fillable= this.fillable, field
			for(var i=0;i<fillable.length;i++){
				field= fillable[i]
				this.__createGetter(field)
			}
		}
	}

	static set database(db){
		this.$database= db
	}

	static get database(){
		if(!this.$database)
			this.$database= Varavel.Database.Database.current
		return this.$database
	}

	set database(db){
		this.$database= db
	}

	get database(){
		if(!this.$database)
			this.$database= Varavel.Database.Database.current
		return this.$database
	}



	__createGetter(field){
		this.__defineGetter__(field, ()=>{
			return this.get(field)
		})

		this.__defineSetter__(field, (val)=>{
			return this.set(field,val)
		})
	}




	get(field){
		return this.$data[field]
	}


	set(field, val){
		this.$data[field]= val
	}


	// Método save ...
	save(){
		if(this.get("id")){
			return this.update()
		}
		else{
			return this.create()
		}
	}


	filterFields(nuevo){

		var fillable= this.fillable
		var tocreate= {}
		for(var id in this.$data){
			tocreate[id]= this.$data[id]
			if(fillable && fillable.indexOf(id)<0){
				delete tocreate[id]
			}
		}

		delete tocreate["created_at"]
		delete tocreate["updated_at"]


		if(this.timestamps==undefined || this.timestamps){
			tocreate["updated_at"]= this.database.nowExpression
			if(nuevo)
				tocreate["created_at"]= tocreate.updated_at
		}

		return tocreate

	}


	async update(){


		var q= this.database.getQueryBuilder(this)
		var fields= this.filterFields()
		vw.log(fields)
		q.update().table(this.table)
			.setFields(fields)
			//.set("updated_at", this.database.nowExpression)
			.where("id=?", this.get("id"))

		//	vw.info(q.toSql())
		await this.database.executeQuery(q)
	}


	async ["delete"](){
		var q= this.database.getQueryBuilder(this)
		q.delete().from(this.table)
			.where("id=?", this.get("id"))

		await this.database.executeQuery(q)
	}

	async create(){
		var q= this.database.getQueryBuilder(this)
		q.insert().into(this.table)
			.setFields(this.filterFields())

		await this.database.executeQuery(q)
	}


	static async create(data){
		var model= new this()
		model.fill(data)
		model.save()
		return model
	}

	static query(){
		var q= this.database.getQueryBuilder(this)
		this.__applySelect(q)
		return q
	}


	static select(){
		var q= this.database.getQueryBuilder(this)
		q.select().from(this.table)
		return q
	}


	static delete(){
		var q= this.database.getQueryBuilder(this)
		q.delete().from(this.table)
		return q
	}

	static __fill(data){
		var d= new this()
		d.$data=data
		return d
	}

	static __applySelect(q){
		q.select()
		if(this.fillable){
			for(var i=0;i<this.fillable.length;i++){
				q.field(this.fillable[i])
			}
		}
		q.from(this.table)
	}

	static async find(id){

		var q= this.database.getQueryBuilder(this)
		this.__applySelect(q)
		q.where("id=?",id)

		var result= await this.database.executeQuery(q)

		var data= result.rows? result.rows[0]:null

		if(data){
			return this.__fill(data)
		}
	}


	toJSON(){
		return this.$data
	}

	static async findOrFail(id){
		var y=  await this.find(id)
		if(!y)
			throw new Varavel.Database.QueryNotResultsException("La consulta no produjo resultados")

		return y
	}


}
export default Model
