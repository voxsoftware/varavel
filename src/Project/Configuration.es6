var D= core.org.voxsoftware.Varavel.Project
import Path from 'path'
class Configuration extends D.DtConfiguration{


	static get ["default"](){
		if(!this.$default)
			throw new core.System.Exception("Debe establecer la configuración del proyecto")

		return this.$default
	}


	static set ["default"](config){
		this.$default= config
	}


	static start(path){
		this.default= new Configuration(Path.join(path, "env.dt"))
		return this.default
	}



}
export default Configuration
