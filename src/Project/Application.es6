import Path from 'path'
var Fs= core.System.IO.Fs
var Varavel= core.org.voxsoftware.Varavel

class Application {

	static  async start(path){

		Application.load(path)
		var config= Application.loadConfig(path)

		// Iniciar el server ...
		var tcpServer= new Varavel.Http.TcpServer()
		tcpServer.config= config

		Application.current.tcpServer= tcpServer
		await tcpServer.init()
	}


	static loadConfig(path){
		var config= Varavel.Project.Configuration.start(path)
		Application.current= new Application()
		Application.current.configuration= config
		return config
	}



	static load(path){
		var obj= {
			"org":{
				"voxsoftware":{
					"Varavel":{
						"Project": {
							"Current": {
								"readPath": path
							}
						}
					}
				}
			}
		}

		// Revisar directorios
		var revisar= (obj, src)=>{
			var stat, ufile, dirs= Fs.sync.readdir(src), file
			for(var i=0; i<dirs.length;i++){
				file= dirs[i]
				ufile= Path.join(src, file)
				stat= Fs.sync.stat(ufile)
				if(stat.isDirectory()){
					obj[file]= {
						readPath : ufile
					}
					revisar(obj[file], ufile)
				}
			}
		}

		revisar(obj.org.voxsoftware.Varavel.Project.Current, path)
		require("./_project-module").org= obj.org


		var Module= new core.VW.Module(path)
		Module.loadConfigFile(Path.join(__dirname, "./_project-module.js"))
		Module.import()
		global.Varavel.Project= core.org.voxsoftware.Varavel.Project.Current



		var m={}, Modules= global.Varavel.Project.Modules
		if(Modules){


			for(var id in Modules){
				Application.__createGetter(m, id)
			}
			global.Varavel.Modules= m
		}




	}


	static __createGetter(m, id){
		m.__defineGetter__(id.toLowerCase(), function(){
			//vw.log("here")
			var t= global.Varavel.Project.Modules[id]
			//vw.log("here2")
			return t
		})
	}

}
export default Application
