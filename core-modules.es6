


import Path from 'path'
var Fs= core.System.IO.Fs
var obj= {
	"org":{
		"voxsoftware":{
			"Varavel":{
				"readPath": "./dist"
			}
		}
	}
}


// Revisar directorios
var revisar= (obj, src)=>{
	var stat, ufile, dirs= Fs.sync.readdir(src), file
	for(var i=0; i<dirs.length;i++){
		file= dirs[i]
		ufile= Path.join(src, file)
		stat= Fs.sync.stat(ufile)
		if(stat.isDirectory()){
			obj[file]= {
				readPath : ufile
			}
			revisar(obj[file], ufile)
		}
	}	
}


revisar(obj.org.voxsoftware.Varavel, Path.join(__dirname, "dist"))
module.exports= obj