var Varavel = core.org.voxsoftware.Varavel;
{
    var Functions = function Functions() {
        Functions.$constructor ? Functions.$constructor.apply(this, arguments) : Functions.$superClass && Functions.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Functions, '$constructor', {
        enumerable: false,
        value: function (args) {
            this.args = args;
        }
    });
    Object.defineProperty(Functions, 'request', {
        enumerable: false,
        value: function (args) {
            return new GlobalFunctions(args);
        }
    });
    Object.defineProperty(Functions.prototype, 'view', {
        enumerable: false,
        value: function (view, args) {
            var view = new Varavel.Resources.View(this.args, view);
            return view.render(args);
        }
    });
    Object.defineProperty(Functions.prototype, 'public_path', {
        enumerable: false,
        value: function () {
            return global.Varavel.Project.Public ? global.Varavel.Project.Public.__dirname : '';
        }
    });
    Object.defineProperty(Functions.prototype, 'env', {
        enumerable: false,
        value: function (value, defaultValue) {
            var val = core.org.voxsoftware.Varavel.Project.Application.current.configuration.get(value);
            if (val == undefined)
                val = defaultValue;
            return val;
        }
    });
}
exports.default = Functions;