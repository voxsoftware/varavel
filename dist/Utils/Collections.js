{
    var Collections = function Collections() {
        Collections.$constructor ? Collections.$constructor.apply(this, arguments) : Collections.$superClass && Collections.$superClass.apply(this, arguments);
    };
    Collections.__defineGetter__('List', function () {
        return require('collections/list');
    });
    Collections.__defineGetter__('Array', function () {
        var _Array = require('collections/shim-array');
        require('collections/listen/array-changes');
        return _Array;
    });
}
exports.default = Collections;