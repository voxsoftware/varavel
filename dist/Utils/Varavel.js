var Varavel = core.org.voxsoftware.Varavel;
{
    var GVaravel = function GVaravel() {
        GVaravel.$constructor ? GVaravel.$constructor.apply(this, arguments) : GVaravel.$superClass && GVaravel.$superClass.apply(this, arguments);
    };
    Object.defineProperty(GVaravel, 'Functions', {
        enumerable: false,
        value: function (args) {
            return new Varavel.Utils.Functions(args);
        }
    });
    GVaravel.__defineGetter__('Funcs', function () {
        return this.Functions();
    });
}
exports.default = GVaravel;