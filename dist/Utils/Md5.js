{
    var Md5 = function Md5() {
        Md5.$constructor ? Md5.$constructor.apply(this, arguments) : Md5.$superClass && Md5.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Md5, 'encrypt', {
        enumerable: false,
        value: function (pass) {
            if (!Md5.$md5)
                Md5.$md5 = require('./_md5.js');
            return Md5.$md5.apply(Md5.$md5, arguments);
        }
    });
}
exports.default = Md5;