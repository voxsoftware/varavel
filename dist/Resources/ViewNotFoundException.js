{
    var ViewNotFoundException = function ViewNotFoundException() {
        ViewNotFoundException.$constructor ? ViewNotFoundException.$constructor.apply(this, arguments) : ViewNotFoundException.$superClass && ViewNotFoundException.$superClass.apply(this, arguments);
    };
    ViewNotFoundException.prototype = Object.create(core.System.Exception.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(ViewNotFoundException, core.System.Exception) : ViewNotFoundException.__proto__ = core.System.Exception;
    ViewNotFoundException.prototype.constructor = ViewNotFoundException;
    ViewNotFoundException.$super = core.System.Exception.prototype;
    ViewNotFoundException.$superClass = core.System.Exception;
}
exports.default = ViewNotFoundException;