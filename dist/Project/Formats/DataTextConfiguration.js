var $mod$24 = core.VW.Ecma2015.Utils.module(require('path'));
var D = core.org.voxsoftware.Developing;
var Fs = core.System.IO.Fs;
{
    var DataTextConfiguration = function DataTextConfiguration() {
        DataTextConfiguration.$constructor ? DataTextConfiguration.$constructor.apply(this, arguments) : DataTextConfiguration.$superClass && DataTextConfiguration.$superClass.apply(this, arguments);
    };
    DataTextConfiguration.prototype = Object.create(D.Configuration.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(DataTextConfiguration, D.Configuration) : DataTextConfiguration.__proto__ = D.Configuration;
    DataTextConfiguration.prototype.constructor = DataTextConfiguration;
    DataTextConfiguration.$super = D.Configuration.prototype;
    DataTextConfiguration.$superClass = D.Configuration;
    Object.defineProperty(DataTextConfiguration.prototype, 'toDataText', {
        enumerable: false,
        value: function (dt) {
            var file = this.filename;
            if (dt) {
                file = $mod$24.default.relative(dt.directoryName, file);
            }
            return '@@file ' + file;
        }
    });
    Object.defineProperty(DataTextConfiguration, 'toDataText', {
        enumerable: false,
        value: function (dt, dt2) {
            var file = dt.filename;
            if (dt2) {
                file = $mod$24.default.relative(dt2.directoryName, file);
            }
            return '@@file ' + file;
        }
    });
}
exports.default = DataTextConfiguration;