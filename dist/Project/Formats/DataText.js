var $mod$23 = core.VW.Ecma2015.Utils.module(require('path'));
var D = core.org.voxsoftware.Developing;
var Fs = core.System.IO.Fs;
{
    var DataText = function DataText() {
        DataText.$constructor ? DataText.$constructor.apply(this, arguments) : DataText.$superClass && DataText.$superClass.apply(this, arguments);
    };
    Object.defineProperty(DataText.prototype, 'read', {
        enumerable: false,
        value: function (file) {
            var text = Fs.sync.readFile(file, 'utf8');
            var lines = text.split('\n');
            this.$file = file;
            this.$j = {};
            for (var i = 0; i < lines.length; i++) {
                if (!lines[i].startsWith('#'))
                    this.readLine(lines[i]);
            }
            return this.$j;
        }
    });
    Object.defineProperty(DataText, 'save', {
        enumerable: false,
        value: function (dt) {
            var str = [], val, st, noc, n, dataText = [];
            var revisar = function (j, prefix) {
                var keys;
                if (j instanceof Array) {
                    keys = new Array(j.length);
                    str.push(prefix + ':@@array');
                } else {
                    keys = Object.keys(j);
                }
                for (var i = 0; i < keys.length; i++) {
                    var id = keys[i] || i.toString();
                    n = prefix + (prefix ? '->' : '') + id;
                    noc = false;
                    if (!id.startsWith('$')) {
                        val = j[id];
                        if (!val) {
                            st = '';
                        } else if (val.toDataText) {
                            st = val.toDataText(dt);
                        } else if (val instanceof D.Configuration) {
                            st = D.Formats.DataTextConfiguration.toDataText(val, dt);
                        } else if (typeof val === 'object') {
                            revisar(val, n);
                            noc = true;
                        } else {
                            st = val.toString();
                        }
                        if (!noc) {
                            str.push(n + ':' + st);
                        }
                    }
                }
            };
            var revisar2 = function (j) {
                var keys;
                if (j instanceof Array)
                    keys = new Array(j.length);
                else
                    keys = Object.keys(j);
                for (var i = 0; i < keys.length; i++) {
                    var id = keys[i] || i.toString();
                    if (!id.startsWith('$')) {
                        val = j[id];
                        if (val instanceof D.Configuration) {
                            dataText.push(val);
                        } else if (typeof val === 'object') {
                            revisar2(val);
                        }
                    }
                }
            };
            if (dt.$edited) {
                revisar(dt.$.json, '');
                Fs.sync.writeFile(dt.filename, str.join('\n'));
                dt.$edited = false;
            }
            revisar2(dt.$.json);
            if (dataText.length > 0) {
                for (var i = 0; i < dataText.length; i++) {
                    DataText.save(dataText[i]);
                }
            }
        }
    });
    Object.defineProperty(DataText.prototype, 'createGetter', {
        enumerable: false,
        value: function (j, lpart, config, file) {
            var f, name = '$.' + lpart;
            return function () {
                if (config && !f) {
                    j[name] = new D.Formats.DataTextConfiguration(file);
                    f = true;
                }
                return j[name];
            };
        }
    });
    Object.defineProperty(DataText.prototype, 'createSetter', {
        enumerable: false,
        value: function (j, lpart) {
            var name = '$.' + lpart;
            return function (value) {
                var root = j;
                while (!(root instanceof D.Formats.DataTextConfiguration) && root && root._parent) {
                    root = root.$parent;
                }
                if (root instanceof D.Formats.DataTextConfiguration) {
                    root.$edited = true;
                }
                j[name] = value;
            };
        }
    });
    Object.defineProperty(DataText.prototype, 'readLine', {
        enumerable: false,
        value: function (str) {
            str = str.trim();
            var i = str.indexOf(':');
            var id, val, parts, j, fromFile, h, array;
            if (i >= 0) {
                id = str.substring(0, i);
                val = str.substring(i + 1);
                parts = id.split('->');
                j = this.$j;
                for (var i = 0; i < parts.length - 1; i++) {
                    h = j[parts[i]] = j[parts[i]] || {};
                    Object.defineProperty(h, '$parent', {
                        enumerable: false,
                        writable: true,
                        value: j
                    });
                    j = h;
                }
                val = val.trim();
                lpart = parts[parts.length - 1];
                fromFile = val.startsWith('@@file');
                array = val.startsWith('@@array');
                if (array) {
                    j['$.' + lpart] = [];
                    j.__defineGetter__(lpart, this.createGetter(j, lpart));
                    j.__defineSetter__(lpart, this.createSetter(j, lpart));
                } else if (fromFile) {
                    val = val.substring(7);
                    val = $mod$23.default.resolve($mod$23.default.dirname(this.$file), val);
                    j.__defineGetter__(lpart, this.createGetter(j, lpart, true, val));
                    j.__defineSetter__(lpart, this.createSetter(j, lpart));
                } else {
                    j['$.' + lpart] = val.trim();
                    j.__defineGetter__(lpart, this.createGetter(j, lpart));
                    j.__defineSetter__(lpart, this.createSetter(j, lpart));
                }
            }
        }
    });
}
exports.default = DataText;