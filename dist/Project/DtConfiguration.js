var $mod$21 = core.VW.Ecma2015.Utils.module(require('path'));
var $mod$22 = core.VW.Ecma2015.Utils.module(require('events'));
var Fs = core.System.IO.Fs;
var D = core.org.voxsoftware.Varavel.Project;
{
    var Configuration = function Configuration() {
        Configuration.$constructor ? Configuration.$constructor.apply(this, arguments) : Configuration.$superClass && Configuration.$superClass.apply(this, arguments);
    };
    Configuration.prototype = Object.create($mod$22.EventEmitter.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Configuration, $mod$22.EventEmitter) : Configuration.__proto__ = $mod$22.EventEmitter;
    Configuration.prototype.constructor = Configuration;
    Configuration.$super = $mod$22.EventEmitter.prototype;
    Configuration.$superClass = $mod$22.EventEmitter;
    Object.defineProperty(Configuration, 'require', {
        enumerable: false,
        value: function (module) {
            var ext = $mod$21.default.extname(module);
            if (ext == '.dt') {
                return new D.Formats.DataText().read(module);
            }
            Configuration.cache = Configuration.cache || {};
            var file = require.resolve(module);
            if (!file)
                throw new core.System.IO.FileNotFoundException(file);
            var stat = Fs.sync.stat(file);
            var c = Configuration.cache[file] = Configuration.cache[file] || {};
            if (stat.mtime.getTime() != c.mtime) {
                delete require.cache[file];
            }
            c.mtime = stat.mtime.getTime();
            return require(file);
        }
    });
    Object.defineProperty(Configuration, '$constructor', {
        enumerable: false,
        value: function (fileOrJson, parent) {
            Configuration.$superClass.call(this);
            this.$ = this.$ || {};
            if (typeof fileOrJson == 'string') {
                var stat = Fs.sync.stat(fileOrJson);
                if (stat.isDirectory())
                    fileOrJson = $mod$21.default.join(fileOrJson, this.defaultFileName || 'configuration.json');
                this.$.isDt = fileOrJson.endsWith('.dt');
                this.$.json = Configuration.require(fileOrJson);
                if (this.$.json.default)
                    this.$.json = this.$.json.default;
                this.$.file = fileOrJson;
            } else {
                this.$.json = fileOrJson;
            }
            this.$.parent = parent;
        }
    });
    Object.defineProperty(Configuration.prototype, 'createGetter', {
        enumerable: false,
        value: function (id) {
            this.__defineGetter__(id, function (self$0) {
                return function () {
                    return self$0.get(id);
                };
            }(this));
        }
    });
    Object.defineProperty(Configuration.prototype, 'createSetter', {
        enumerable: false,
        value: function (id) {
            this.__defineSetter__(id, function (self$0) {
                return function (val) {
                    return self$0.set(id, val);
                };
            }(this));
        }
    });
    Object.defineProperty(Configuration.prototype, 'get', {
        enumerable: false,
        value: function (name) {
            var j = this.$.json;
            var i, parts = name.split('.');
            i = 0;
            while (j && i < parts.length) {
                if (j instanceof Configuration)
                    j = j.get(parts[i]);
                else
                    j = j[parts[i]];
                i++;
            }
            return j;
        }
    });
    Object.defineProperty(Configuration.prototype, 'set', {
        enumerable: false,
        value: function (name, val) {
            var j = this.$.json;
            var parts = name.split('.'), part;
            for (var i = 0; i < parts.length; i++) {
                part = parts[i];
                if (i == parts.length - 1)
                    j[part] = val;
                else if (!j[part])
                    j[part] = {};
                j = j[part];
            }
            this.$edited = true;
            return this;
        }
    });
    Configuration.prototype.__defineGetter__('parent', function () {
        return this.$.parent;
    });
    Configuration.prototype.__defineGetter__('root', function () {
        var root = null, p = this;
        while (p) {
            root = p;
            p = p.parent;
        }
        return root;
    });
    Configuration.prototype.__defineGetter__('filename', function () {
        return this.$.file;
    });
    Configuration.prototype.__defineGetter__('directoryName', function () {
        if (!this.$.dir) {
            this.$.dir = $mod$21.default.dirname(this.filename);
        }
        return this.$.dir;
    });
    Object.defineProperty(Configuration.prototype, 'toJSON', {
        enumerable: false,
        value: function () {
            return this.$.json;
        }
    });
    Object.defineProperty(Configuration.prototype, 'save', {
        enumerable: false,
        value: function () {
            if (this.$.isDt) {
                return D.Formats.DataText.save(this);
            }
            var file = this.filename;
            var p = this.parent, lp;
            if (!file) {
                while (!file && p) {
                    file = p.filename;
                    if (file)
                        lp = p;
                    p = p.parent;
                }
                if (lp)
                    return lp.save();
            }
            if (!file)
                throw new core.System.Exception('No se encontró el archivo de configuración');
            var data = core.safeJSON.stringify(this, null, '\t');
            if (file.endsWith('.es6') || file.endsWith('.js')) {
                data = 'module.exports= ' + data;
            } else if (!file.endsWith('.json')) {
                throw new core.System.NotImplementedException('El formato del archivo' + file + ' no es soportado para guardar.');
            }
            Fs.sync.writeFile(file, data);
            return this;
        }
    });
}
exports.default = Configuration;