var D = core.org.voxsoftware.Varavel.Project;
var $mod$20 = core.VW.Ecma2015.Utils.module(require('path'));
{
    var Configuration = function Configuration() {
        Configuration.$constructor ? Configuration.$constructor.apply(this, arguments) : Configuration.$superClass && Configuration.$superClass.apply(this, arguments);
    };
    Configuration.prototype = Object.create(D.DtConfiguration.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Configuration, D.DtConfiguration) : Configuration.__proto__ = D.DtConfiguration;
    Configuration.prototype.constructor = Configuration;
    Configuration.$super = D.DtConfiguration.prototype;
    Configuration.$superClass = D.DtConfiguration;
    Configuration.__defineGetter__('default', function () {
        if (!this.$default)
            throw new core.System.Exception('Debe establecer la configuración del proyecto');
        return this.$default;
    });
    Configuration.__defineSetter__('default', function (config) {
        this.$default = config;
    });
    Object.defineProperty(Configuration, 'start', {
        enumerable: false,
        value: function (path) {
            this.default = new Configuration($mod$20.default.join(path, 'env.dt'));
            return this.default;
        }
    });
}
exports.default = Configuration;