var $mod$19 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var Varavel = core.org.voxsoftware.Varavel;
{
    var Application = function Application() {
        Application.$constructor ? Application.$constructor.apply(this, arguments) : Application.$superClass && Application.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Application, 'start', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(path) {
            var config, tcpServer;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        Application.load(path);
                        config = Application.loadConfig(path);
                        tcpServer = new Varavel.Http.TcpServer();
                        tcpServer.config = config;
                        Application.current.tcpServer = tcpServer;
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(tcpServer.init());
                    case 7:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Application, 'loadConfig', {
        enumerable: false,
        value: function (path) {
            var config = Varavel.Project.Configuration.start(path);
            Application.current = new Application();
            Application.current.configuration = config;
            return config;
        }
    });
    Object.defineProperty(Application, 'load', {
        enumerable: false,
        value: function (path) {
            var obj = { 'org': { 'voxsoftware': { 'Varavel': { 'Project': { 'Current': { 'readPath': path } } } } } };
            var revisar = function (obj, src) {
                var stat, ufile, dirs = Fs.sync.readdir(src), file;
                for (var i = 0; i < dirs.length; i++) {
                    file = dirs[i];
                    ufile = $mod$19.default.join(src, file);
                    stat = Fs.sync.stat(ufile);
                    if (stat.isDirectory()) {
                        obj[file] = { readPath: ufile };
                        revisar(obj[file], ufile);
                    }
                }
            };
            revisar(obj.org.voxsoftware.Varavel.Project.Current, path);
            require('./_project-module').org = obj.org;
            var Module = new core.VW.Module(path);
            Module.loadConfigFile($mod$19.default.join(__dirname, './_project-module.js'));
            Module.import();
            global.Varavel.Project = core.org.voxsoftware.Varavel.Project.Current;
            var m = {}, Modules = global.Varavel.Project.Modules;
            if (Modules) {
                for (var id in Modules) {
                    Application.__createGetter(m, id);
                }
                global.Varavel.Modules = m;
            }
        }
    });
    Object.defineProperty(Application, '__createGetter', {
        enumerable: false,
        value: function (m, id) {
            m.__defineGetter__(id.toLowerCase(), function () {
                var t = global.Varavel.Project.Modules[id];
                return t;
            });
        }
    });
}
exports.default = Application;