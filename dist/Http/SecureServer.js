var $mod$8 = core.VW.Ecma2015.Utils.module(require('events'));
var $mod$9 = core.VW.Ecma2015.Utils.module(require('path'));
var $mod$10 = core.VW.Ecma2015.Utils.module(require('url'));
var $mod$11 = core.VW.Ecma2015.Utils.module(require('tls'));
var Varavel = core.org.voxsoftware.Varavel;
var fsSync = core.System.IO.Fs.sync;
{
    var Server = function Server() {
        Server.$constructor ? Server.$constructor.apply(this, arguments) : Server.$superClass && Server.$superClass.apply(this, arguments);
    };
    Server.prototype = Object.create($mod$8.EventEmitter.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Server, $mod$8.EventEmitter) : Server.__proto__ = $mod$8.EventEmitter;
    Server.prototype.constructor = Server;
    Server.$super = $mod$8.EventEmitter.prototype;
    Server.$superClass = $mod$8.EventEmitter;
    Object.defineProperty(Server.prototype, 'awaitListen', {
        enumerable: false,
        value: function () {
            var task = new core.VW.Task();
            this.once('listen', function () {
                task.finish();
            });
            return task;
        }
    });
    Object.defineProperty(Server.prototype, 'awaitFirstInit', {
        enumerable: false,
        value: function () {
            if (this.$inited)
                return;
            return this.awaitInit();
        }
    });
    Object.defineProperty(Server.prototype, 'awaitInit', {
        enumerable: false,
        value: function () {
            var task = new core.VW.Task();
            this.once('init', function () {
                task.finish();
            });
            return task;
        }
    });
    Server.prototype.__defineGetter__('console', function () {
        return this.tcpServer.console;
    });
    Object.defineProperty(Server.prototype, 'close', {
        enumerable: false,
        value: function () {
            return this.server.close();
        }
    });
    Server.prototype.__defineGetter__('server', function () {
        return this.$server;
    });
    Object.defineProperty(Server.prototype, 'init', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(throwOnError) {
            var c, server;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        c = this.console;
                        context$1$0.prev = 1;
                        server = new core.VW.E6Html.Http.SecureServer(this.secureOptions);
                        this.$server = server;
                        server.timeout = this.config.get('SERVER_TIMEOUT') || 4500;
                        server.port = 0;
                        server.useBodyParser = true;
                        this.initServer();
                        this.emit('init');
                        context$1$0.next = 11;
                        return regeneratorRuntime.awrap(server.listen());
                    case 11:
                        this.emit('listen');
                        this.console.log('Server HTTPS disponible: 127.0.0.1:', server.port);
                        this.initListen();
                        context$1$0.next = 21;
                        break;
                    case 16:
                        context$1$0.prev = 16;
                        context$1$0.t0 = context$1$0['catch'](1);
                        if (!throwOnError) {
                            context$1$0.next = 20;
                            break;
                        }
                        throw context$1$0.t0;
                    case 20:
                        this.console.error(context$1$0.t0);
                    case 21:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    16
                ]]);
        })
    });
    Object.defineProperty(Server.prototype, 'initListen', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var reqArgs;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!true) {
                            context$1$0.next = 7;
                            break;
                        }
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(this.$server.acceptAsync());
                    case 3:
                        reqArgs = context$1$0.sent;
                        this._continue(reqArgs);
                        context$1$0.next = 0;
                        break;
                    case 7:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Server.prototype, 'initServer', {
        enumerable: false,
        value: function () {
            var router = this.server.router;
            var App = global.Varavel.Project.App;
            if (App.Http && App.Http.Routes) {
                App.Http.Routes(router);
            }
            var Modules = global.Varavel.Project.Modules, modul;
            if (Modules) {
                for (var id in Modules) {
                    modul = Modules[id];
                    if (modul.Http && modul.Http.Routes)
                        modul.Http.Routes(router);
                }
            }
        }
    });
    Object.defineProperty(Server.prototype, '_continue', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(req) {
            var time, id;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        time = new Date();
                        id = ++Server.id;
                        req.id = id;
                        context$1$0.prev = 3;
                        req.request.url = req.request.url.trim();
                        this.console.request(req.request.method, req.request.url, ' ID: ', id);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(req.catch(req.continue));
                    case 8:
                        this.console.info('Solicitud HTTPS completa: ', id, ' Tiempo: ', new Date() - time, 'ms');
                        context$1$0.next = 14;
                        break;
                    case 11:
                        context$1$0.prev = 11;
                        context$1$0.t0 = context$1$0['catch'](3);
                        try {
                            req.response.statusCode = 500;
                            try {
                                req.response.write(JSON.stringify({ error: context$1$0.t0.stack }, 4, '\t'));
                            } catch (ex) {
                            }
                            req.response.end();
                        } catch (ex) {
                        }
                    case 14:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    3,
                    11
                ]]);
        })
    });
}
Server.id = 0;
exports.default = Server;