var $mod$16 = core.VW.Ecma2015.Utils.module(require('net'));
var Varavel = core.org.voxsoftware.Varavel;
var $mod$17 = core.VW.Ecma2015.Utils.module(require('tls'));
var $mod$18 = core.VW.Ecma2015.Utils.module(require('events'));
var LE = undefined;
{
    var TcpServer = function TcpServer() {
        TcpServer.$constructor ? TcpServer.$constructor.apply(this, arguments) : TcpServer.$superClass && TcpServer.$superClass.apply(this, arguments);
    };
    TcpServer.prototype = Object.create($mod$18.EventEmitter.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(TcpServer, $mod$18.EventEmitter) : TcpServer.__proto__ = $mod$18.EventEmitter;
    TcpServer.prototype.constructor = TcpServer;
    TcpServer.$super = $mod$18.EventEmitter.prototype;
    TcpServer.$superClass = $mod$18.EventEmitter;
    Object.defineProperty(TcpServer.prototype, 'init', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(throwOnError) {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!(this.config.get('CREATE_TCP') == 1)) {
                            context$1$0.next = 3;
                            break;
                        }
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(this.createConn());
                    case 3:
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(this.renewHttpServer());
                    case 5:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    TcpServer.prototype.__defineGetter__('httpServer', function () {
        if (!this.$httpServer)
            this.$httpServer = new Varavel.Http.Server();
        this.$httpServer.config = this.config;
        this.$httpServer.tcpServer = this;
        return this.$httpServer;
    });
    Object.defineProperty(TcpServer.prototype, 'renewHttpServer', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var actual, newServer;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!this.$renewing) {
                            context$1$0.next = 2;
                            break;
                        }
                        return context$1$0.abrupt('return', false);
                    case 2:
                        this.$renewing = true;
                        actual = this.$httpServer;
                        newServer = new Varavel.Http.Server();
                        newServer.config = this.config;
                        newServer.tcpServer = this;
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(newServer.init());
                    case 9:
                        this.$httpServer = newServer;
                        if (!actual) {
                            context$1$0.next = 13;
                            break;
                        }
                        context$1$0.next = 13;
                        return regeneratorRuntime.awrap(actual.close());
                    case 13:
                        this.$renewing = false;
                        return context$1$0.abrupt('return', true);
                    case 15:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    TcpServer.prototype.__defineGetter__('server', function () {
        return this.$server;
    });
    Object.defineProperty(TcpServer.prototype, 'createConn', {
        enumerable: false,
        value: function () {
            var task = new core.VW.Task(), server;
            server = $mod$16.default.createServer(this.tcpConnection.bind(this));
            server.on('error', function (self$0) {
                return function (e) {
                    self$0.console.error(e);
                };
            }(this));
            this.$server = server;
            var port = process.env.SERVER_PORT || this.config.get('SERVER_PORT');
            port = parseInt(port);
            server.listen(port, function (self$0) {
                return function (err) {
                    if (err)
                        task.exception = err;
                    self$0.$port = server.address().port;
                    self$0.console.log('Server TCP disponible: 127.0.0.1:', port);
                    task.finish();
                };
            }(this));
            return task;
        }
    });
    TcpServer.prototype.__defineGetter__('port', function () {
        if (this.$port !== undefined)
            return this.$port;
        return this.$httpServer.$server.innerServer.address().port;
    });
    TcpServer.prototype.__defineGetter__('certificate', function () {
        if (!this.$certificate) {
            this.$certificate = new Varavel.Http.Certificate(this.config);
            this.$certificate.config = this.config;
        }
        return this.$certificate;
    });
    Object.defineProperty(TcpServer.prototype, 'getHttpsServer', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var hostname, self, iHttps, credentials;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        return context$1$0.abrupt('return');
                    case 6:
                        credentials = context$1$0.sent;
                        vw.info(credentials);
                        context$1$0.prev = 8;
                        if (iHttps) {
                            context$1$0.next = 24;
                            break;
                        }
                        iHttps = new Varavel.Http.SecureServer();
                        iHttps.config = this.config;
                        this.$httpsServer = iHttps;
                        iHttps.hostname = hostname;
                        iHttps.secureOptions = credentials;
                        iHttps.secureOptions.SNICallback = function (hostname, cb) {
                            var parts = hostname.split('.');
                            if (parts.length > 2) {
                                parts[0] = '*';
                            }
                            parts = parts.join('.');
                            try {
                                if (cb) {
                                    self.certificate.getForHostname(parts).then(function (credentials) {
                                        var context = $mod$17.default.createSecureContext(credentials);
                                        return cb(null, context);
                                    }).catch(function (err) {
                                        return cb(err);
                                    });
                                } else {
                                    var credentials2 = self.certificate.getCacheForHostName(parts);
                                    var context = $mod$17.default.createSecureContext(credentials2);
                                    return context;
                                }
                            } catch (e) {
                                self.console.error(e);
                            }
                        };
                        setImmediate(function () {
                            iHttps.init();
                        });
                        context$1$0.next = 19;
                        return regeneratorRuntime.awrap(iHttps.awaitInit());
                    case 19:
                        self.httpServer.$proxy.addServer(iHttps);
                        context$1$0.next = 22;
                        return regeneratorRuntime.awrap(iHttps.awaitListen());
                    case 22:
                        context$1$0.next = 26;
                        break;
                    case 24:
                        context$1$0.next = 26;
                        return regeneratorRuntime.awrap(iHttps.awaitFirstInit());
                    case 26:
                        context$1$0.next = 32;
                        break;
                    case 28:
                        context$1$0.prev = 28;
                        context$1$0.t0 = context$1$0['catch'](8);
                        self.console.error(context$1$0.t0);
                        return context$1$0.abrupt('return', false);
                    case 32:
                        return context$1$0.abrupt('return', iHttps);
                    case 33:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    8,
                    28
                ]]);
        })
    });
    TcpServer.prototype.__defineGetter__('console', function () {
        if (!this.$console)
            this.$console = new Varavel.Http.Console();
        return this.$console;
    });
    Object.defineProperty(TcpServer.prototype, 'tcpConnection', {
        enumerable: false,
        value: function (conn) {
            var self = this;
            conn.on('error', function (e) {
                self.console.error(e);
            });
            conn.once('data', function (buf) {
                var address;
                var continuar = function () {
                    var proxy = $mod$16.default.createConnection(address, function () {
                        proxy.setTimeout && proxy.setTimeout(0);
                        proxy.write(buf, function () {
                            conn.pipe(proxy).pipe(conn);
                        });
                    });
                    proxy.on('error', function (er) {
                        vw.error('Error no controlado en el socket: ', er);
                    });
                };
                var useHttps = self.config.get('USE_HTTPS') == 1;
                if (!useHttps || buf[0] != 22) {
                    address = self.httpServer.server.port;
                    continuar();
                } else {
                    self.getHttpsServer().then(function (httpsServer) {
                        address = httpsServer.server.port;
                        continuar();
                    }).catch(function (err) {
                        vw.error(err);
                    });
                }
            });
        }
    });
}
exports.default = TcpServer;