var $mod$5 = core.VW.Ecma2015.Utils.module(require('squel'));
{
    var Query = function Query() {
        Query.$constructor ? Query.$constructor.apply(this, arguments) : Query.$superClass && Query.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Query, '$constructor', {
        enumerable: false,
        value: function (modelClass) {
            this.modelClass = modelClass;
            this.translations = {};
        }
    });
    Object.defineProperty(Query.prototype, 'createQuery', {
        enumerable: false,
        value: function (args) {
            var selectArg = this.translations['select'] || 'fields';
            var q;
            if (!args.fields) {
                this.defaultSelect();
                q = this.q;
            } else {
                q = this.q = this.modelClass.select();
            }
        }
    });
    Object.defineProperty(Query.prototype, 'defaultSelect', {
        enumerable: false,
        value: function () {
            this.q = this.modelClass.query();
        }
    });
    Object.defineProperty(Query.prototype, 'group', {
        enumerable: false,
        value: function (value) {
            if (this.validGroup && !this.validGroup(value))
                return;
            this.q.group(value);
            this.$grouped = true;
        }
    });
    Object.defineProperty(Query.prototype, 'where', {
        enumerable: false,
        value: function (value) {
            var procesar = function (value) {
                if (typeof value == 'object') {
                    var expr = $mod$5.default.expr(), f, arg, time = 0;
                    if (value.type == 'and') {
                        f = 'and';
                    } else {
                        f = 'or';
                    }
                    {
                        typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined;
                        var item;
                        var $_iterator1 = regeneratorRuntime.values(value.conditions), $_normal1 = false, $_err1;
                        try {
                            while (true) {
                                var $_step1 = $_iterator1.next();
                                if ($_step1.done) {
                                    $_normal1 = true;
                                    break;
                                }
                                {
                                    item = $_step1.value;
                                    arg = procesar(item);
                                    if (arg) {
                                        expr[f].apply(expr, arg);
                                        time++;
                                    }
                                }
                            }
                        } catch (e) {
                            $_normal1 = false;
                            $_err1 = e;
                        }
                        try {
                            if (!$_normal1 && $_iterator1['return']) {
                                $_iterator1['return']();
                            }
                        } catch (e) {
                        }
                        if ($_err1) {
                            throw $_err1;
                        }
                    }
                    if (!time)
                        return null;
                    return [expr];
                } else {
                    if (this.validWhere && !this.validWhere(value))
                        return null;
                    return value;
                }
            };
            var arg = procesar(value);
            if (arg)
                this.q.where(arg);
            this.$filtered = true;
        }
    });
    Object.defineProperty(Query.prototype, 'join', {
        enumerable: false,
        value: function (value) {
            this.$joined = true;
        }
    });
    Object.defineProperty(Query.prototype, 'field', {
        enumerable: false,
        value: function (value) {
            if (this.validField && !this.validField(value))
                return null;
            this.q.field(value);
            this.$selected = true;
        }
    });
    Object.defineProperty(Query.prototype, 'limit', {
        enumerable: false,
        value: function (value) {
            if (this.validLimit && !this.validLimit(value))
                return null;
            this.q.limit(value);
            this.$limited = true;
        }
    });
    Object.defineProperty(Query.prototype, 'prequery', {
        enumerable: false,
        value: function () {
        }
    });
    Object.defineProperty(Query.prototype, 'postquery', {
        enumerable: false,
        value: function () {
        }
    });
}
exports.default = Query;