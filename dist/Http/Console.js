{
    var Console = function Console() {
        Console.$constructor ? Console.$constructor.apply(this, arguments) : Console.$superClass && Console.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Console.prototype, 'write', {
        enumerable: false,
        value: function (type, args, special) {
            var t, noline;
            if (type == 'info') {
                t = 'INFO';
                core.VW.Console.backgroundColor = core.System.ConsoleColor.Blue;
                core.VW.Console.foregroundColor = core.System.ConsoleColor.White;
            } else if (type == 'warning') {
                t = 'WARNING';
                core.VW.Console.backgroundColor = core.System.ConsoleColor.Yellow;
                core.VW.Console.foregroundColor = core.System.ConsoleColor.Black;
            } else if (type == 'log') {
                t = 'LOG';
                core.VW.Console.backgroundColor = core.System.ConsoleColor.Green;
                core.VW.Console.foregroundColor = core.System.ConsoleColor.White;
            } else if (type == 'error') {
                t = 'ERROR';
                core.VW.Console.backgroundColor = core.System.ConsoleColor.Red;
                core.VW.Console.foregroundColor = core.System.ConsoleColor.White;
            }
            if (special) {
                t = special;
            }
            core.VW.Console.write('', t, '').resetColors().write(' ');
            for (var i = 0; i < args.length; i++) {
                msg = args[i];
                if (msg instanceof Error) {
                    core.VW.Console.write(msg.message, msg.stack).writeLine();
                    for (var id in msg) {
                        core.VW.Console.writeLine(' - ' + id, msg[id]);
                    }
                } else if (Buffer.isBuffer(msg)) {
                    core.VW.Console.backgroundColor = core.System.ConsoleColor.Magenta;
                    core.VW.Console.foregroundColor = core.System.ConsoleColor.Black;
                    core.VW.Console.write('Buffer', '').resetColors();
                    core.VW.Console.write(msg.toString('ascii'));
                } else {
                    core.VW.Console.write(msg);
                }
            }
            if (!noline)
                core.VW.Console.writeLine();
        }
    });
    Object.defineProperty(Console.prototype, 'request', {
        enumerable: false,
        value: function (method, args) {
            args = Array.prototype.slice.call(arguments, 1);
            return this.write('log', args, method);
        }
    });
    Object.defineProperty(Console.prototype, 'error', {
        enumerable: false,
        value: function () {
            return this.write('error', arguments);
        }
    });
    Object.defineProperty(Console.prototype, 'warning', {
        enumerable: false,
        value: function () {
            return this.write('warning', arguments);
        }
    });
    Object.defineProperty(Console.prototype, 'info', {
        enumerable: false,
        value: function () {
            return this.write('info', arguments);
        }
    });
    Object.defineProperty(Console.prototype, 'log', {
        enumerable: false,
        value: function () {
            return this.write('log', arguments);
        }
    });
}
exports.default = Console;