{
    var Controller = function Controller() {
        Controller.$constructor ? Controller.$constructor.apply(this, arguments) : Controller.$superClass && Controller.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Controller, 'bindMethod', {
        enumerable: false,
        value: function (name) {
            var self = this, er;
            return function () {
                var c = new self();
                if (arguments[0] && arguments[0].response) {
                    arguments[0].request.on('error', function (e) {
                        vw.error(e);
                        arguments[0].response.end();
                    });
                }
                return c[name].apply(c, arguments);
            };
        }
    });
}
exports.default = Controller;