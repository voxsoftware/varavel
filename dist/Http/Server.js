var $mod$12 = core.VW.Ecma2015.Utils.module(require('events'));
var $mod$13 = core.VW.Ecma2015.Utils.module(require('path'));
var $mod$14 = core.VW.Ecma2015.Utils.module(require('url'));
var $mod$15 = core.VW.Ecma2015.Utils.module(require('tls'));
var Varavel = core.org.voxsoftware.Varavel;
var fsSync = core.System.IO.Fs.sync;
{
    var Server = function Server() {
        Server.$constructor ? Server.$constructor.apply(this, arguments) : Server.$superClass && Server.$superClass.apply(this, arguments);
    };
    Server.prototype = Object.create($mod$12.EventEmitter.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Server, $mod$12.EventEmitter) : Server.__proto__ = $mod$12.EventEmitter;
    Server.prototype.constructor = Server;
    Server.$super = $mod$12.EventEmitter.prototype;
    Server.$superClass = $mod$12.EventEmitter;
    Server.prototype.__defineGetter__('console', function () {
        return this.tcpServer.console;
    });
    Object.defineProperty(Server.prototype, 'close', {
        enumerable: false,
        value: function () {
            return this.server.close();
        }
    });
    Server.prototype.__defineGetter__('server', function () {
        return this.$server;
    });
    Object.defineProperty(Server.prototype, 'init', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(throwOnError) {
            var og, v, ogw, oge, ogh, c, server, tcp;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        try {
                            og = require('_http_outgoing').OutgoingMessage;
                            vw.warning('Changing default funcs');
                            if (!og._oo) {
                                ogw = og.prototype.write;
                                oge = og.prototype.end;
                                ogh = og.prototype.setHeader;
                                og.prototype.write = function () {
                                    if (this.finished)
                                        return console.error('VARAVEL CATCH:Rejecting error after write');
                                    try {
                                        v = ogw.apply(this, arguments);
                                    } catch (e) {
                                        console.error('VARAVEL CATCH: Outgoing message: ', e);
                                    }
                                    return v;
                                };
                                og.prototype.setHeader = function () {
                                    if (this.finished)
                                        return console.error('VARAVEL CATCH:Rejecting error after write');
                                    try {
                                        v = ogh.apply(this, arguments);
                                    } catch (e) {
                                        console.error('VARAVEL CATCH: Outgoing message: ', e);
                                    }
                                    return v;
                                };
                                og.prototype.end = function () {
                                    try {
                                        v = oge.apply(this, arguments);
                                    } catch (e) {
                                        console.error('VARAVEL CATCH: Outgoing message: ', e);
                                    }
                                    return v;
                                };
                                og._oo = true;
                            }
                        } catch (e) {
                            vw.error(e);
                        }
                        c = this.console;
                        context$1$0.prev = 2;
                        server = new core.VW.E6Html.Http.Server();
                        this.$server = server;
                        server.timeout = this.config.get('SERVER_TIMEOUT') || 0;
                        tcp = this.config.get('CREATE_TCP') == 1;
                        server.port = tcp ? 0 : process.env.SERVER_PORT || this.config.get('SERVER_PORT');
                        server.port = parseInt(server.port);
                        server.bodyParserArgs = {
                            limit: '50mb',
                            extended: false
                        };
                        if (global.Varavel.Project.Public)
                            server.path = global.Varavel.Project.Public.__dirname;
                        server.useBodyParser = this.config.get('USE_BODY_PARSER', 0) == 1;
                        server.useBodyParser = false;
                        context$1$0.next = 15;
                        return regeneratorRuntime.awrap(this.initServer());
                    case 15:
                        this.emit('init');
                        this.initListen();
                        context$1$0.next = 19;
                        return regeneratorRuntime.awrap(server.listen());
                    case 19:
                        this.emit('listen');
                        this.console.log('Server HTTP disponible: 127.0.0.1:', server.port);
                        context$1$0.next = 28;
                        break;
                    case 23:
                        context$1$0.prev = 23;
                        context$1$0.t0 = context$1$0['catch'](2);
                        if (!throwOnError) {
                            context$1$0.next = 27;
                            break;
                        }
                        throw context$1$0.t0;
                    case 27:
                        this.console.error(context$1$0.t0);
                    case 28:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    2,
                    23
                ]]);
        })
    });
    Object.defineProperty(Server.prototype, 'initListen', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        this.$server.acceptDelegate = this._continue.bind(this);
                    case 1:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Server.prototype, 'initServer', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var router, App, Modules, modul, id;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        router = this.server.router;
                        App = global.Varavel.Project.App;
                        if (App.Http && App.Http.Routes) {
                            App.Http.Routes(router);
                        }
                        Modules = global.Varavel.Project.Modules;
                        if (!Modules) {
                            context$1$0.next = 14;
                            break;
                        }
                        context$1$0.t0 = regeneratorRuntime.keys(Modules);
                    case 6:
                        if ((context$1$0.t1 = context$1$0.t0()).done) {
                            context$1$0.next = 14;
                            break;
                        }
                        id = context$1$0.t1.value;
                        modul = Modules[id];
                        if (!(modul.Http && modul.Http.Routes)) {
                            context$1$0.next = 12;
                            break;
                        }
                        context$1$0.next = 12;
                        return regeneratorRuntime.awrap(modul.Http.Routes(router));
                    case 12:
                        context$1$0.next = 6;
                        break;
                    case 14:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Server.prototype, '_continue', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(req) {
            var time, id;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        time = new Date();
                        id = ++Server.id;
                        req.id = id;
                        context$1$0.prev = 3;
                        req.request.url = req.request.url.trim();
                        if (!global.Varavel || global.Varavel.Funcs.env('SHOW_INREQUEST'))
                            this.console.request(req.request.method, req.request.url, ' ID: ', id);
                        req.request.on('destroy', function (e) {
                            console.error('VARAVEL CATCH: secure destry: ', e);
                        });
                        req.request.on('error', function (e) {
                            console.error('VARAVEL CATCH:', e);
                        });
                        req.response.on('error', function (e) {
                            console.error('VARAVEL CATCH:', e);
                        });
                        context$1$0.next = 11;
                        return regeneratorRuntime.awrap(req.catch(req.continueAsync));
                    case 11:
                        this.console.info('Solicitud completa: ' + id + ' URL: ', req.request.url, ' Tiempo: ' + (new Date() - time) + 'ms');
                        context$1$0.next = 17;
                        break;
                    case 14:
                        context$1$0.prev = 14;
                        context$1$0.t0 = context$1$0['catch'](3);
                        try {
                            req.response.statusCode = 500;
                            try {
                                req.response.write(JSON.stringify({ error: context$1$0.t0.stack }, 4, '\t'));
                            } catch (ex) {
                            }
                            req.response.end();
                        } catch (ex) {
                        }
                    case 17:
                        req.dispose && req.dispose();
                    case 18:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    3,
                    14
                ]]);
        })
    });
}
Server.id = 0;
exports.default = Server;