var $mod$6 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var $mod$7 = core.VW.Ecma2015.Utils.module(require('child_process'));
{
    var Certificate = function Certificate() {
        Certificate.$constructor ? Certificate.$constructor.apply(this, arguments) : Certificate.$superClass && Certificate.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Certificate, '$constructor', {
        enumerable: false,
        value: function () {
            this.$certs = {};
        }
    });
    Object.defineProperty(Certificate.prototype, 'getForHostname', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(hostname, email) {
            var opts, certs, o;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!this.$certs[hostname]) {
                            context$1$0.next = 2;
                            break;
                        }
                        return context$1$0.abrupt('return', this.$certs[hostname]);
                    case 2:
                        vw.warning(hostname);
                        opts = {
                            domains: [hostname],
                            email: email || this.config.get('EMAIL') || 'server@local.dev',
                            agreeTos: true
                        };
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(Le.register(opts));
                    case 6:
                        certs = context$1$0.sent;
                        o = {
                            key: certs[0],
                            cert: certs[1]
                        };
                        this.$certs[hostname] = o;
                        return context$1$0.abrupt('return', o);
                    case 10:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = Certificate;