{
    var JsonResponse = function JsonResponse() {
        JsonResponse.$constructor ? JsonResponse.$constructor.apply(this, arguments) : JsonResponse.$superClass && JsonResponse.$superClass.apply(this, arguments);
    };
    Object.defineProperty(JsonResponse, '$constructor', {
        enumerable: false,
        value: function (args) {
            this.args = args;
        }
    });
    Object.defineProperty(JsonResponse.prototype, 'parse', {
        enumerable: false,
        value: function (key, value) {
            var v = this[key];
            if (Buffer.isBuffer(v)) {
                return {
                    'type': 'base64',
                    'value': v.toString('base64')
                };
            }
            return value;
        }
    });
    Object.defineProperty(JsonResponse.prototype, 'write', {
        enumerable: false,
        value: function (arg) {
            var data = arg, er;
            if (arg && arg.stack) {
                data = {
                    message: arg.message,
                    stack: arg.stack
                };
                for (var id in arg) {
                    data[id] = arg[id];
                }
                data = { error: data };
                er = true;
            }
            data = core.safeJSON.stringify(data, this.parse, '\t');
            if (er)
                this.args.response.statusCode = 500;
            else
                this.args.response.statusCode = 200;
            this.args.response.setHeader('Content-type', 'application/json; Charset=utf-8');
            this.args.response.write(data ? data : 'null');
            this.args.response.end();
        }
    });
}
exports.default = JsonResponse;