var $mod$0 = core.VW.Ecma2015.Utils.module(require('mysql'));
var $mod$1 = core.VW.Ecma2015.Utils.module(require('squel'));
var Varavel = core.org.voxsoftware.Varavel;
var inited = false;
{
    var Database = function Database() {
        Database.$constructor ? Database.$constructor.apply(this, arguments) : Database.$superClass && Database.$superClass.apply(this, arguments);
    };
    Database.prototype = Object.create(Varavel.Database.Database.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Database, Varavel.Database.Database) : Database.__proto__ = Varavel.Database.Database;
    Database.prototype.constructor = Database;
    Database.$super = Varavel.Database.Database.prototype;
    Database.$superClass = Varavel.Database.Database;
    Object.defineProperty(Database, '$constructor', {
        enumerable: false,
        value: function (config) {
            this.$mysql = $mod$0.default.createPool({
                connectionLimit: config.get('DB_CONNECTIONLIMIT') || 10,
                host: config.get('DB_HOST') || '127.0.0.1',
                user: config.get('DB_USER') || 'root',
                password: config.get('DB_PASSWORD') || '',
                port: config.get('DB_PORT') || 3306,
                database: config.get('DB_DATABASE') || 3306
            });
            this.useUTC = true;
            if (!inited) {
                $mod$1.default.registerValueHandler(Date, function (value) {
                    var m = core.VW.Moment(value);
                    var str = 'DD.MM.YYYY HH.mm.ss';
                    return $mod$1.default.expr('STR_TO_DATE(\'' + str + '\', \'%d.%m.%Y %H.%i.%s\')');
                });
                $mod$1.default.registerValueHandler(core.VW.Moment, function (m) {
                    var str = 'DD.MM.YYYY HH.mm.ss';
                    return $mod$1.default.expr('STR_TO_DATE(\'' + str + '\', \'%d.%m.%Y %H.%i.%s\')');
                });
                inited = true;
            }
        }
    });
    Database.__defineGetter__('mysql', function () {
        if (Database.$mysql)
            return Database.$mysql;
        return Database.$mysql = require('mysql');
    });
    Database.prototype.__defineGetter__('nowExpression', function () {
        if (this.useUTC)
            return this.nowUTCExpression;
        return $mod$1.default.str('now()');
    });
    Database.prototype.__defineGetter__('nowUTCExpression', function () {
        return $mod$1.default.str('utc_timestamp()');
    });
    Database.prototype.__defineGetter__('queryBuilder', function () {
        return new Varavel.Database.MySql.QueryBuilder(this);
    });
    Object.defineProperty(Database.prototype, 'executeQuery', {
        enumerable: false,
        value: function (queryBuilder) {
            var param = queryBuilder.toParam ? queryBuilder.toParam() : queryBuilder.toString();
            if (param.text)
                param.sql = param.text;
            var task = new core.VW.Task();
            this.$mysql.query(param, function (err, results, fields) {
                if (err)
                    task.exception = err;
                var o = {};
                o.rows = results;
                o.fields = fields;
                task.result = o;
                task.finish();
            });
            return task;
        }
    });
}
exports.default = Database;