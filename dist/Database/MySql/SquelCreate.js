var $mod$3 = core.VW.Ecma2015.Utils.module(require('squel'));
{
    var CreateTableBlock = function CreateTableBlock() {
        CreateTableBlock.$constructor ? CreateTableBlock.$constructor.apply(this, arguments) : CreateTableBlock.$superClass && CreateTableBlock.$superClass.apply(this, arguments);
    };
    CreateTableBlock.prototype = Object.create($mod$3.default.cls.Block.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(CreateTableBlock, $mod$3.default.cls.Block) : CreateTableBlock.__proto__ = $mod$3.default.cls.Block;
    CreateTableBlock.prototype.constructor = CreateTableBlock;
    CreateTableBlock.$super = $mod$3.default.cls.Block.prototype;
    CreateTableBlock.$superClass = $mod$3.default.cls.Block;
    Object.defineProperty(CreateTableBlock.prototype, 'table', {
        enumerable: false,
        value: function (name) {
            this._name = name;
        }
    });
    Object.defineProperty(CreateTableBlock.prototype, '_toParamString', {
        enumerable: false,
        value: function (options) {
            return {
                text: this._name,
                values: []
            };
        }
    });
}
{
    var CreateFieldBlock = function CreateFieldBlock() {
        CreateFieldBlock.$constructor ? CreateFieldBlock.$constructor.apply(this, arguments) : CreateFieldBlock.$superClass && CreateFieldBlock.$superClass.apply(this, arguments);
    };
    CreateFieldBlock.prototype = Object.create($mod$3.default.cls.Block.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(CreateFieldBlock, $mod$3.default.cls.Block) : CreateFieldBlock.__proto__ = $mod$3.default.cls.Block;
    CreateFieldBlock.prototype.constructor = CreateFieldBlock;
    CreateFieldBlock.$super = $mod$3.default.cls.Block.prototype;
    CreateFieldBlock.$superClass = $mod$3.default.cls.Block;
    Object.defineProperty(CreateFieldBlock, '$constructor', {
        enumerable: false,
        value: function (options) {
            CreateFieldBlock.$superClass.call(this, options);
            this._fields = [];
        }
    });
    Object.defineProperty(CreateFieldBlock.prototype, 'field', {
        enumerable: false,
        value: function (name, type) {
            this._fields.push({
                name: name,
                type: type
            });
        }
    });
    Object.defineProperty(CreateFieldBlock.prototype, '_toParamString', {
        enumerable: false,
        value: function (options) {
            var str$1 = this._fields.map(function (f) {
                return '' + f.name + ' ' + f.type.toUpperCase() + '';
            }).join(', ');
            return {
                text: '(' + str$1 + ')',
                values: []
            };
        }
    });
}
{
    var CreateTableQuery = function CreateTableQuery() {
        CreateTableQuery.$constructor ? CreateTableQuery.$constructor.apply(this, arguments) : CreateTableQuery.$superClass && CreateTableQuery.$superClass.apply(this, arguments);
    };
    CreateTableQuery.prototype = Object.create($mod$3.default.cls.QueryBuilder.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(CreateTableQuery, $mod$3.default.cls.QueryBuilder) : CreateTableQuery.__proto__ = $mod$3.default.cls.QueryBuilder;
    CreateTableQuery.prototype.constructor = CreateTableQuery;
    CreateTableQuery.$super = $mod$3.default.cls.QueryBuilder.prototype;
    CreateTableQuery.$superClass = $mod$3.default.cls.QueryBuilder;
    Object.defineProperty(CreateTableQuery, '$constructor', {
        enumerable: false,
        value: function (options, blocks) {
            CreateTableQuery.$superClass.call(this, options, blocks || [
                new $mod$3.default.cls.StringBlock(options, 'CREATE TABLE'),
                new CreateTableBlock(options),
                new CreateFieldBlock(options)
            ]);
        }
    });
}
$mod$3.default.create = function (options) {
    return new CreateTableQuery(options);
};