var $mod$2 = core.VW.Ecma2015.Utils.module(require('squel'));
var Varavel = core.org.voxsoftware.Varavel;
var none = Varavel.Database.MySql.SquelCreate;
{
    var QueryBuilder = function QueryBuilder() {
        QueryBuilder.$constructor ? QueryBuilder.$constructor.apply(this, arguments) : QueryBuilder.$superClass && QueryBuilder.$superClass.apply(this, arguments);
    };
    QueryBuilder.prototype = Object.create(Varavel.Database.QueryBuilder.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(QueryBuilder, Varavel.Database.QueryBuilder) : QueryBuilder.__proto__ = Varavel.Database.QueryBuilder;
    QueryBuilder.prototype.constructor = QueryBuilder;
    QueryBuilder.$super = Varavel.Database.QueryBuilder.prototype;
    QueryBuilder.$superClass = Varavel.Database.QueryBuilder;
    Object.defineProperty(QueryBuilder.prototype, '_engine', {
        enumerable: false,
        value: function () {
            return $mod$2.default.useFlavour('mysql');
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'create', {
        enumerable: false,
        value: function () {
            this.$sql = $mod$2.default.create();
        }
    });
}
exports.default = QueryBuilder;