{
    var QueryNotResultsException = function QueryNotResultsException() {
        QueryNotResultsException.$constructor ? QueryNotResultsException.$constructor.apply(this, arguments) : QueryNotResultsException.$superClass && QueryNotResultsException.$superClass.apply(this, arguments);
    };
    QueryNotResultsException.prototype = Object.create(core.System.Exception.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(QueryNotResultsException, core.System.Exception) : QueryNotResultsException.__proto__ = core.System.Exception;
    QueryNotResultsException.prototype.constructor = QueryNotResultsException;
    QueryNotResultsException.$super = core.System.Exception.prototype;
    QueryNotResultsException.$superClass = core.System.Exception;
}
exports.default = QueryNotResultsException;