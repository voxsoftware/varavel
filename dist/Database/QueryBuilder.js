var $mod$4 = core.VW.Ecma2015.Utils.module(require('squel'));
var VaravelNamespace = core.org.voxsoftware.Varavel;
{
    var QueryBuilder = function QueryBuilder() {
        QueryBuilder.$constructor ? QueryBuilder.$constructor.apply(this, arguments) : QueryBuilder.$superClass && QueryBuilder.$superClass.apply(this, arguments);
    };
    Object.defineProperty(QueryBuilder, '$constructor', {
        enumerable: false,
        value: function (db) {
            this.db = db;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'select', {
        enumerable: false,
        value: function () {
            this.$sql = this._engine().select();
            return this;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'create', {
        enumerable: false,
        value: function () {
            throw new core.System.NotImplementedException();
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'update', {
        enumerable: false,
        value: function () {
            this.$sql = this._engine().update();
            return this;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'delete', {
        enumerable: false,
        value: function () {
            this.$sql = this._engine().delete();
            return this;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'insert', {
        enumerable: false,
        value: function () {
            this.$sql = this._engine().insert();
            return this;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, '_engine', {
        enumerable: false,
        value: function () {
            return $mod$4.default;
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'first', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.get());
                    case 2:
                        return context$1$0.abrupt('return', context$1$0.sent[0]);
                    case 3:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(QueryBuilder.prototype, 'get', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var result, rows, ret, row, $_iterator0, $_normal0, $_err0, $_step0;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.db.executeQuery(this));
                    case 2:
                        result = context$1$0.sent;
                        rows = result.rows;
                        ret = new VaravelNamespace.Utils.Collections.Array();
                        if (!rows) {
                            context$1$0.next = 27;
                            break;
                        }
                        typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined;
                        $_iterator0 = regeneratorRuntime.values(rows), $_normal0 = false;
                        context$1$0.prev = 8;
                    case 9:
                        if (!true) {
                            context$1$0.next = 18;
                            break;
                        }
                        $_step0 = $_iterator0.next();
                        if (!$_step0.done) {
                            context$1$0.next = 14;
                            break;
                        }
                        $_normal0 = true;
                        return context$1$0.abrupt('break', 18);
                    case 14:
                        row = $_step0.value;
                        ret.push(this.modelClass.__fill(row));
                        context$1$0.next = 9;
                        break;
                    case 18:
                        context$1$0.next = 24;
                        break;
                    case 20:
                        context$1$0.prev = 20;
                        context$1$0.t0 = context$1$0['catch'](8);
                        $_normal0 = false;
                        $_err0 = context$1$0.t0;
                    case 24:
                        try {
                            if (!$_normal0 && $_iterator0['return']) {
                                $_iterator0['return']();
                            }
                        } catch (e) {
                        }
                        if (!$_err0) {
                            context$1$0.next = 27;
                            break;
                        }
                        throw $_err0;
                    case 27:
                        return context$1$0.abrupt('return', ret);
                    case 28:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    8,
                    20
                ]]);
        })
    });
    Object.defineProperty(QueryBuilder.prototype, 'execute', {
        enumerable: false,
        value: function () {
            throw new core.System.NotImplementedException();
        }
    });
    Object.defineProperty(QueryBuilder.prototype, 'clone', {
        enumerable: false,
        value: function () {
            var sql = this.$sql.clone();
            var d = Object.getPrototypeOf ? Object.getPrototypeOf(this) : this.__proto__;
            var c = new d();
            c.$sql = sql;
            return c;
        }
    });
    Object.defineProperty(QueryBuilder, 'processArguments', {
        enumerable: false,
        value: function (args) {
            for (var i = 0; i < args.length; i++) {
                if (args[i] instanceof QueryBuilder)
                    args[i] = args[i].$sql;
            }
            return args;
        }
    });
}
var props = [
    'where',
    'toParam',
    'toString',
    'into',
    'from',
    'table',
    'returning',
    'distinct',
    'field',
    'join',
    'outer_join',
    'left_join',
    'right_join',
    'expr',
    'having',
    'group',
    'limit',
    'offset',
    'union',
    'function',
    'set',
    'setFields',
    'order',
    'and',
    'or',
    'with',
    'str'
];
var createFunc = function (prop) {
    QueryBuilder.prototype[prop] = function () {
        var func = this.$sql[prop];
        var args = QueryBuilder.processArguments(arguments);
        var r = func.apply(this.$sql, args);
        if (r == this.$sql)
            r = this;
        return r;
    };
};
props.forEach(createFunc);
exports.default = QueryBuilder;