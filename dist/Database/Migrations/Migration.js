var VaravelNamespace = core.org.voxsoftware.Varavel;
var Migrations = VaravelNamespace.Database.Migrations;
{
    var Migration = function Migration() {
        Migration.$constructor ? Migration.$constructor.apply(this, arguments) : Migration.$superClass && Migration.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Migration.prototype, 'up', {
        enumerable: false,
        value: function () {
        }
    });
    Object.defineProperty(Migration.prototype, 'down', {
        enumerable: false,
        value: function () {
        }
    });
    Object.defineProperty(Migration.prototype, 'run', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var migration;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(Migrations.Entities.Migration.select().where('migration', Object.getPrototypeOf(this).__filename).first());
                    case 2:
                        migration = context$1$0.sent;
                        if (!(!migration || !migration.batch == 0)) {
                            context$1$0.next = 11;
                            break;
                        }
                        if (!migration)
                            migration = new Migrations.Entities.Migration();
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(this.up());
                    case 7:
                        migration.set('migration', Object.getPrototypeOf(this).__filename);
                        migration.set('batch', 1);
                        context$1$0.next = 11;
                        return regeneratorRuntime.awrap(migration.save());
                    case 11:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Migration.prototype, 'remove', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var migration;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(Migrations.Entities.Migration.select().where('migration', Object.getPrototypeOf(this).__filename).first());
                    case 2:
                        migration = context$1$0.sent;
                        if (!migration) {
                            context$1$0.next = 9;
                            break;
                        }
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.down());
                    case 6:
                        migration.set('batch', 0);
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(migration.save());
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Migration.prototype, 'verifyTable', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var migration, create, query;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        try {
                            migration = Migrations.Entities.Migration.find(1);
                        } catch (e) {
                            create = true;
                        }
                        query = VaravelNamespace.Database.Database.current.queryBuilder;
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(query.create().table('migration').field('migration', 'varchar(400)').field('batch', 'integer').execute());
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = Migration;