var VaravelNamespace = core.org.voxsoftware.Varavel;
{
    var Migration = function Migration() {
        Migration.$constructor ? Migration.$constructor.apply(this, arguments) : Migration.$superClass && Migration.$superClass.apply(this, arguments);
    };
    Migration.prototype = Object.create(VaravelNamespace.Database.Model.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Migration, VaravelNamespace.Database.Model) : Migration.__proto__ = VaravelNamespace.Database.Model;
    Migration.prototype.constructor = Migration;
    Migration.$super = VaravelNamespace.Database.Model.prototype;
    Migration.$superClass = VaravelNamespace.Database.Model;
    Object.defineProperty(Migration, '$constructor', {
        enumerable: false,
        value: function () {
            this.table = 'migration';
            this.fillable = [
                'migration',
                'batch'
            ];
            Migration.$superClass.call(this);
        }
    });
}
Migration.table = 'migration';
exports.default = Migration;