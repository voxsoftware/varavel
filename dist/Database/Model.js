var Varavel = core.org.voxsoftware.Varavel;
{
    var Model = function Model() {
        Model.$constructor ? Model.$constructor.apply(this, arguments) : Model.$superClass && Model.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Model, '$constructor', {
        enumerable: false,
        value: function () {
            this.$data = {};
            if (this.fillable) {
                var fillable = this.fillable, field;
                for (var i = 0; i < fillable.length; i++) {
                    field = fillable[i];
                    this.__createGetter(field);
                }
            }
        }
    });
    Model.__defineSetter__('database', function (db) {
        this.$database = db;
    });
    Model.__defineGetter__('database', function () {
        if (!this.$database)
            this.$database = Varavel.Database.Database.current;
        return this.$database;
    });
    Model.prototype.__defineSetter__('database', function (db) {
        this.$database = db;
    });
    Model.prototype.__defineGetter__('database', function () {
        if (!this.$database)
            this.$database = Varavel.Database.Database.current;
        return this.$database;
    });
    Object.defineProperty(Model.prototype, '__createGetter', {
        enumerable: false,
        value: function (field) {
            this.__defineGetter__(field, function (self$0) {
                return function () {
                    return self$0.get(field);
                };
            }(this));
            this.__defineSetter__(field, function (self$0) {
                return function (val) {
                    return self$0.set(field, val);
                };
            }(this));
        }
    });
    Object.defineProperty(Model.prototype, 'get', {
        enumerable: false,
        value: function (field) {
            return this.$data[field];
        }
    });
    Object.defineProperty(Model.prototype, 'set', {
        enumerable: false,
        value: function (field, val) {
            this.$data[field] = val;
        }
    });
    Object.defineProperty(Model.prototype, 'save', {
        enumerable: false,
        value: function () {
            if (this.get('id')) {
                return this.update();
            } else {
                return this.create();
            }
        }
    });
    Object.defineProperty(Model.prototype, 'filterFields', {
        enumerable: false,
        value: function (nuevo) {
            var fillable = this.fillable;
            var tocreate = {};
            for (var id in this.$data) {
                tocreate[id] = this.$data[id];
                if (fillable && fillable.indexOf(id) < 0) {
                    delete tocreate[id];
                }
            }
            delete tocreate['created_at'];
            delete tocreate['updated_at'];
            if (this.timestamps == undefined || this.timestamps) {
                tocreate['updated_at'] = this.database.nowExpression;
                if (nuevo)
                    tocreate['created_at'] = tocreate.updated_at;
            }
            return tocreate;
        }
    });
    Object.defineProperty(Model.prototype, 'update', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var q, fields;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        q = this.database.getQueryBuilder(this);
                        fields = this.filterFields();
                        vw.log(fields);
                        q.update().table(this.table).setFields(fields).where('id=?', this.get('id'));
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.database.executeQuery(q));
                    case 6:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Model.prototype, 'delete', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var q;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        q = this.database.getQueryBuilder(this);
                        q.delete().from(this.table).where('id=?', this.get('id'));
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(this.database.executeQuery(q));
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Model.prototype, 'create', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var q;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        q = this.database.getQueryBuilder(this);
                        q.insert().into(this.table).setFields(this.filterFields());
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(this.database.executeQuery(q));
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Model, 'create', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(data) {
            var model;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        model = new this();
                        model.fill(data);
                        model.save();
                        return context$1$0.abrupt('return', model);
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Model, 'query', {
        enumerable: false,
        value: function () {
            var q = this.database.getQueryBuilder(this);
            this.__applySelect(q);
            return q;
        }
    });
    Object.defineProperty(Model, 'select', {
        enumerable: false,
        value: function () {
            var q = this.database.getQueryBuilder(this);
            q.select().from(this.table);
            return q;
        }
    });
    Object.defineProperty(Model, 'delete', {
        enumerable: false,
        value: function () {
            var q = this.database.getQueryBuilder(this);
            q.delete().from(this.table);
            return q;
        }
    });
    Object.defineProperty(Model, '__fill', {
        enumerable: false,
        value: function (data) {
            var d = new this();
            d.$data = data;
            return d;
        }
    });
    Object.defineProperty(Model, '__applySelect', {
        enumerable: false,
        value: function (q) {
            q.select();
            if (this.fillable) {
                for (var i = 0; i < this.fillable.length; i++) {
                    q.field(this.fillable[i]);
                }
            }
            q.from(this.table);
        }
    });
    Object.defineProperty(Model, 'find', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(id) {
            var q, result, data;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        q = this.database.getQueryBuilder(this);
                        this.__applySelect(q);
                        q.where('id=?', id);
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(this.database.executeQuery(q));
                    case 5:
                        result = context$1$0.sent;
                        data = result.rows ? result.rows[0] : null;
                        if (!data) {
                            context$1$0.next = 9;
                            break;
                        }
                        return context$1$0.abrupt('return', this.__fill(data));
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Model.prototype, 'toJSON', {
        enumerable: false,
        value: function () {
            return this.$data;
        }
    });
    Object.defineProperty(Model, 'findOrFail', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(id) {
            var y;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.find(id));
                    case 2:
                        y = context$1$0.sent;
                        if (y) {
                            context$1$0.next = 5;
                            break;
                        }
                        throw new Varavel.Database.QueryNotResultsException('La consulta no produjo resultados');
                    case 5:
                        return context$1$0.abrupt('return', y);
                    case 6:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = Model;