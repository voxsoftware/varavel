var Varavel = core.org.voxsoftware.Varavel;
{
    var Database = function Database() {
        Database.$constructor ? Database.$constructor.apply(this, arguments) : Database.$superClass && Database.$superClass.apply(this, arguments);
    };
    Database.__defineGetter__('current', function () {
        var config = Varavel.Project.Configuration.default;
        var tipo = config.get('DB_CONNECTION');
        if (tipo == 'mysql') {
            return new Varavel.Database.MySql.Database(config);
        } else {
            throw new core.System.NotImplementedException(tipo);
        }
    });
    Database.prototype.__defineGetter__('nowExpression', function () {
        if (this.useUTC)
            return this.nowUTCExpression;
        return new Date();
    });
    Database.prototype.__defineGetter__('nowUTCExpression', function () {
        return new Date().toISOString();
    });
    Database.prototype.__defineGetter__('queryBuilder', function () {
        return new Varavel.Database.QueryBuilder(this);
    });
    Object.defineProperty(Database.prototype, 'getQueryBuilder', {
        enumerable: false,
        value: function (model) {
            var e = this.queryBuilder;
            e.modelClass = model;
            return e;
        }
    });
    Object.defineProperty(Database.prototype, 'executeQuery', {
        enumerable: false,
        value: function () {
            throw new core.System.NotImplementedException();
        }
    });
}
exports.default = Database;